# README #

Ein Beispiel wie in PlatformIo eine Library aufgebaut wird.

## Referenz: ##
https://docs.platformio.org/en/latest/librarymanager/creating.html

## Use: ##
Library einbinden:
Dazu öffne platformio.ini und füge deinem Environment den Eintrag lib_deps hinzu.

>lib_deps = https://bitbucket.org/3EA-REPO/tea_pio_lib_helloworld.git

Existiert lib_deps schon mit einem Eintrag z.B. durch eine verwendete PID Library, fügst du die Gitadresse dieser Library nur noch als eingerückte Zeile hinzu.
Also ungefähr so:

>lib_deps =
>>br3ttb/PID@^1.2.1

>>https://bitbucket.org/3EA-REPO/tea_pio_lib_helloworld.git
	
Speicher die Änderungen und drücke auf Projekt erstellen. Jetzt wird die Library automatisch von unserem Bitbucket Server gezogen. Die Abhängigkeiten, im Beispiel hier die arduinoCLI Library, werden automatisch mit Installiert.

In examples findet sich ein Beispiel zur verwendung der Library.

