/**
 * @file main.cpp
 * @author Michael Wolkstein (mw@3rd-element.com)
 * @brief 
 * @version 0.1
 * @date 2021-09-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <Arduino.h>
#include <Tea_HelloWorld.h>

// Create one TEA_HELLO_WORLD objekt with LED_BUILDIN 
TEA_HELLO_WORLD MyHW(LED_BUILTIN, 100);

void setup() {
  Serial.begin(9600);
  // call TEA_HELLO_WORLD Init function
  MyHW.init();
}

void loop() {
  // call the TEA_HELLO_WORLD
  MyHW.testblink();
}

// Hihi v1.3.0