/**
 * @file HelloWorld.cpp
 * @author Michael Wolkstein (mw@3rd-element.com)
 * @brief 
 * @version 1.0.0
 * @date 2021-09-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "Tea_HelloWorld.h"

/**
 * @brief Construct a new tea hello world::tea hello world object
 * 
 * @param ledPin 
 * @param timeinterval 
 */
TEA_HELLO_WORLD::TEA_HELLO_WORLD(int ledPin, int timeinterval)
{
    _ledPin = ledPin;
    _interval = timeinterval;
}

/**
 * @brief TEA_HELLO_WORLD initExample
 * 
 */
void TEA_HELLO_WORLD::init()
{
    pinMode(_ledPin, OUTPUT);
	_lastblinkinterval = millis();
}

/**
 * @brief TEA_HELLO_WORLD Blink Led in time
 * 
 */
void TEA_HELLO_WORLD::testblink()
{

    if( millis() >= _lastblinkinterval + _interval){
	    digitalWrite(_ledPin, !digitalRead(_ledPin));
        _lastblinkinterval = millis();
	}
    _timeleft = _interval - (millis() - _lastblinkinterval);
}

/**
 * @brief set a new ledtoggleinterval
 * 
 * @param time 
 */
void TEA_HELLO_WORLD::setNewBlinkInterval(int time)
{
    _interval = time;

}

/**
 * @brief get time left to next led toggle event
 * 
 * @return int 
 */
int TEA_HELLO_WORLD::getTimeLeftNextBlink()
{
    return _timeleft;
}

