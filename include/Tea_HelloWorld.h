/**
 * @file HelloWorld.h
 * @author Michael Wolkstein (mw@3rd-element.com)
 * @brief example library
 * @version 1.0.0
 * @date 2021-09-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef TEA_HELLO_WORLD_H
#define TEA_HELLO_WORLD_H

#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

/**
 * @brief Third Element Aviation Linear Slider Device Class
 * 
 */
class TEA_HELLO_WORLD  
{

  public:

    TEA_HELLO_WORLD(int interval, int ledPin);
    
    void init();
    void testblink();
	  int getTimeLeftNextBlink();
	  void setNewBlinkInterval( int interval);

  private:

	int _ledPin;
  int _interval;
  int _timeleft;
	uint32_t _lastblinkinterval;

};
#endif
